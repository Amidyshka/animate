//Pure JavaScript Animation


    var info = document.getElementById('popupInfo');
    var infoBtn = document.getElementById('infoBtn');
    infoBtn.onmouseover = showPop;
    infoBtn.onmouseout = hidePop;

function showPop() {
 addClass(info, 'open')
}
function hidePop() {
 removeClass(info, 'open')
}


var animation = document.getElementById("animation"),
    slide1 = document.getElementsByClassName("slide1"),
    slide2 = document.getElementsByClassName("slide2"),
    f_h_l = document.getElementById("hand_left_1"),
    f_h_r = document.getElementById("hand_left_1_r"),
    s_h_r = document.getElementById("hand_left_2_r"),
    s_h_l = document.getElementById("hand_left_2"),
    sw = document.getElementById("steering_wheel"),
    first_bg = document.getElementById("first_bg"),
    newff1 = document.getElementById("newff-1"),
    newff2 = document.getElementById("newff-2"),
    newff3 = document.getElementById("newff-3"),
    autos = document.getElementById("autos"),
    tick1 = document.getElementById("tick1"),
    tick2 = document.getElementById("tick2"),
    tick3 = document.getElementById("tick3"),
    tick4 = document.getElementById("tick4"),
    tick5 = document.getElementById("tick5"),
    tick6 = document.getElementById("tick6"),
    tick7 = document.getElementById("tick7"),
    tick8 = document.getElementById("tick8"),
    tick9 = document.getElementById("tick9"),
    tick10 = document.getElementById("tick10"),
    tick11 = document.getElementById("tick11"),
    tick12 = document.getElementById("tick12"),
    tick13 = document.getElementById("tick13"),
    ticks = document.getElementById("ticks"),
    newff13 = document.getElementById("newff-13"),
    newff14 = document.getElementById("newff-14"),
    newff15 = document.getElementById("newff-15"),
    arrow = document.getElementById("arrow"),

tl = new TimelineMax({
    repeat: 0,
    repeatDelay: 3
});

tl  
.to(tick1, 0, {rotation: 0}).to(tick2, 0,{rotation: 8}).to(tick3,0,{rotation: 15}).to(tick4, 0, {rotation: 24}).to(tick5, 0, {rotation: 32}).to(tick6, 0, {rotation: 40}).to(tick7, 0, {rotation: 48}).to(tick8, 0, {rotation: 56}).to(tick9, 0, {    rotation: 57}).to(tick10, 0, {    rotation: 68}).to(tick11, 0, {    rotation: 75}).to(tick12, 0, {    rotation: 78}).to(tick13, 0, {    rotation: 90})
    .to(newff1, 0, {autoAlpha: 0}, 'start_first')
    .to(newff2, 0, {autoAlpha: 0}, 'start_first')
    .to(newff3, 0, {autoAlpha: 0}, 'start_first')

    .to(newff1, 0.7, {autoAlpha: 1}, 'f_h')
    .to(f_h_l, 0.7, { display:'none'}, "f_h" )
    .to(f_h_r, 0.7, { display:'none'}, "f_h")
    .to(s_h_l, 0.4, { display:'block'}, "s_h")
    .to(s_h_r, 0.4, { display:'block'}, "s_h")
    .to(newff2, 0.7, {autoAlpha: 1}, 's_h')

    .to(s_h_l, 1, { x: -80, y: 80 }, "move_s_h")
    .to(s_h_r, 1, { x: 80, y: 80}, "move_s_h")
    .to(sw, 1.2, { rotation: 65}, "move_whs")
    .to([ticks,tick1], 0.1, { autoAlpha: 1, display: 'block'}, "move_whs+=0.1")
    .to([tick2], 0.1, { autoAlpha: 1, display: 'block'}, "move_whs+=0.1")
    .to([tick3], 0.1, { autoAlpha: 1, display: 'block'}, "move_whs+=0.2")
    .to([tick4], 0.1, { autoAlpha: 1, display: 'block'}, "move_whs+=0.3")
    .to([tick5], 0.1, { autoAlpha: 1, display: 'block'}, "move_whs+=0.4")
    .to([tick6], 0.1, { autoAlpha: 1, display: 'block'}, "move_whs+=0.5")
    .to([tick7], 0.1, { autoAlpha: 1, display: 'block'}, "move_whs+=0.6")
    .to([tick8], 0.1, { autoAlpha: 1, display: 'block'}, "move_whs+=0.7")
    .to([tick9], 0.1, { autoAlpha: 1, display: 'block'}, "move_whs+=0.8")
    .to([tick10], 0.1, { autoAlpha: 1, display: 'block'}, "move_whs+=0.9")
    .to([tick11], 0.1, { autoAlpha: 1, display: 'block'}, "move_whs+=0.95")
    .to([tick12], 0.1, { autoAlpha: 1, display: 'block'}, "move_whs+=1.0")
    .to([tick13], 0.1, { autoAlpha: 1, display: 'block'}, "move_whs+=1.1")
    .to(newff3, 0.7, {autoAlpha: 1}, 'move_s_h+=0.7')
    .to(first_bg, 5, { left: 0}, "move_whs-=0.2")
    .to(sw, 0.6, { rotation: 0,ease: Back.easeOut }, "move_sw_back")
    .to([tick13], 0.05, { autoAlpha: 0, display: 'none'},"move_sw_back")
    .to([tick12], 0.05, { autoAlpha: 0, display: 'none'},"move_sw_back+=0.05")
    .to([tick11], 0.05, { autoAlpha: 0, display: 'none'},"move_sw_back+=0.1")
    .to([tick10], 0.05, { autoAlpha: 0, display: 'none'},"move_sw_back+=0.15")
    .to([tick9], 0.05, { autoAlpha: 0, display: 'none'},"move_sw_back+=0.2")
    .to([tick8], 0.05, { autoAlpha: 0, display: 'none'},"move_sw_back+=0.25")
    .to([tick7], 0.05, { autoAlpha: 0, display: 'none'},"move_sw_back+=0.3")
    .to([tick6], 0.05, { autoAlpha: 0, display: 'none'},"move_sw_back+=0.35")
    .to([tick5], 0.05, { autoAlpha: 0, display: 'none'},"move_sw_back+=0.4")
    .to([tick4], 0.05, { autoAlpha: 0, display: 'none'},"move_sw_back+=0.45")
    .to([tick3], 0.05, { autoAlpha: 0, display: 'none'},"move_sw_back+=0.5")
    .to([tick2], 0.05, { autoAlpha: 0, display: 'none'},"move_sw_back+=0.55")
    .to([tick1], 0.05, { autoAlpha: 0, display: 'none'}, "move_sw_back+=0.6")
    .to([ticks], 0.05, { autoAlpha: 0, display: 'none'}, "move_sw_back+=0.65")
    .to(slide1, 1.3, { display: 'none'}, "end_first")

    .to(slide1, 0, { display: 'none'}, "start_second").to(autos, 0, {rotation:-2}, "start_second")
    .to(slide2, 0, { display: 'block'}, "start_second").to(autos, 0, {rotation:-2}, "start_second")
    .to([newff13,newff14,newff15], 0, {autoAlpha: 0}, "start_second")
    .to(wheels_1, 0, {scaleY:1.1})
    .to([newff13], 1, {autoAlpha: 1, display: 'block'},'newff13')
    .to([newff13], 1, {autoAlpha: 0, display: 'block'},'newff13+=3')
    .to([newff14], 1, {autoAlpha: 1, display: 'block'},'newff13+=4')
    .to([newff14], 1, {autoAlpha: 0, display: 'block'},'newff13+=6')
    .to([newff15], 1, {autoAlpha: 1, display: 'block'},'newff13+=7')
    .to(autos, 4, {x: 100, y:-30, scale:0.9}, "newff13")
    .to(wheels_1, 4, {rotation: 360}, "newff13")
    .to(wheels_2, 4, {rotation: 360}, "newff13")
;


TweenLite.set(animation, {
    autoAlpha: 1
});


document.getElementById("pause").onclick = function () {
    tl.pause();
};
document.getElementById("restart").onclick = function () {
    tl.restart();
};
document.getElementById("start").onclick = function () {
    tl.play();
};

function hasClass(el, className) {
  if (el.classList)
    return el.classList.contains(className)
  else
    return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
}

function addClass(el, className) {
  if (el.classList)
    el.classList.add(className)
  else if (!hasClass(el, className)) el.className += " " + className
}

function removeClass(el, className) {
  if (el.classList)
    el.classList.remove(className)
  else if (hasClass(el, className)) {
    var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
    el.className=el.className.replace(reg, ' ')
  }
}


